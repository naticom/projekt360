/* eslint-env browser */
import $ from 'jquery';

const Footer = {
  init() {
    this.catchDOM();
    if (this.$el.length > 0 && this.$box.length > 0) {
      this.catchDOM();
      this.someFunction();
    }
  },
  catchDOM() {
    this.$el = $('.footer');
    this.$element = this.$el.find('.footer__element');
  },
  someFunction() {}
};

export default Footer;
