import slick from 'slick-carousel'

$(document).ready(() => {
  $('.opinion__sliderOpinionBox').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true
  });
}); 