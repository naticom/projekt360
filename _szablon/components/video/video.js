import slick from 'slick-carousel'

$(document).ready(() => {
  $('.video__box').slick({
    autoplay:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false
  });
}); 